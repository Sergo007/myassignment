package sergo.myapplication1;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;
/**
 * Created by Sergo on 01.02.2015.
 */
public class RecyclerListViewAdapter extends RecyclerView.Adapter<RecyclerListViewAdapter.MyViewAppsHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ClickListener clickListener;
    private int layoutInformationID;
    List<InformationApps> data = Collections.emptyList();

    public RecyclerListViewAdapter(Context context, List<InformationApps> data) {
        this.context=context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }
    public void setLayoutInformationID(int layoutInformationID){ this.layoutInformationID=layoutInformationID;}
    public void setClickListener(ClickListener clickListener){this.clickListener=clickListener;}
    @Override
    public MyViewAppsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(layoutInformationID, parent, false);
        MyViewAppsHolder holder = new MyViewAppsHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(MyViewAppsHolder holder, int position) {
        InformationApps current = data.get(position);
        holder.name_app.setText(current.getName_app());
        holder.name_author.setText(current.getName_author());
        holder.price.setText(current.getPrice());
        holder.avatar_app.setImageResource(current.getAvatar_appId());
        holder.rating_app.setRating(current.getRating());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewAppsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name_app;
        TextView name_author;
        TextView price;
        RatingBar rating_app;
        ImageView avatar_app;


        public MyViewAppsHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name_app = (TextView) itemView.findViewById(R.id.name_apps);
            name_author = (TextView) itemView.findViewById(R.id.name_author);
            price = (TextView) itemView.findViewById(R.id.price);
            avatar_app = (ImageView) itemView.findViewById(R.id.avatar_app);
            rating_app = (RatingBar) itemView.findViewById(R.id.rating_app);



        }

        @Override
        public void onClick(View v) {
            if(clickListener!=null) clickListener.itemClick(v,getPosition());
        }
    }
    public interface ClickListener{
        public void itemClick(View v, int position);
    }
}