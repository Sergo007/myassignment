package sergo.myapplication1;



import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sergo.myapplication1.tabs.SlidingTabLayout;


public class MainActivity extends ActionBarActivity {
    private Toolbar toolbar;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drover_layout), toolbar);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setViewPager(mPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.navigate) {
            startActivity(new Intent(this, SubActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
// begin pager adapter
    class MyPagerAdapter extends FragmentPagerAdapter
    {
           String[] tabs;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs=getResources().getStringArray(R.array.tabs);
        }

        @Override
        public Fragment getItem(int position) {
            MyFragment myFragment=MyFragment.getInstance(position);
            return myFragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return 8;
        }
    }
    // end pager adapter
    public static class MyFragment extends Fragment {
        private RecyclerView recyclerView_topPaid;
        private RecyclerView recyclerView_topFree;
        private RecyclerView recyclerView_topGrossing;
        private RecyclerView recyclerView_topNewPaid;
        private RecyclerView recyclerView_topNewFree;
        private RecyclerView recyclerView_Trending;


        public static MyFragment getInstance(int position) {
            MyFragment myFragment = new MyFragment();
            Bundle args = new Bundle();
            args.putInt("position", position);
            myFragment.setArguments(args);
            return myFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View layout;
            RecyclerListViewAdapter adapter_topPaid = new RecyclerListViewAdapter(getActivity(), getData_topPaid());
            RecyclerListViewAdapter adapter_topFree = new RecyclerListViewAdapter(getActivity(), getData_topFree());
            RecyclerListViewAdapter adapter_topGrossing = new RecyclerListViewAdapter(getActivity(), getData_topGrossing());
            RecyclerListViewAdapter adapter_topNewPaid = new RecyclerListViewAdapter(getActivity(), getData_topNewPaid());
            RecyclerListViewAdapter adapter_topNewFree = new RecyclerListViewAdapter(getActivity(), getData_topNewFree());
            RecyclerListViewAdapter adapter_Trending = new RecyclerListViewAdapter(getActivity(), getData_Trending());
            Bundle bundle=getArguments();
            if (bundle.getInt("position")==7)
            {
                layout = inflater.inflate(R.layout.fragment_my6, container, false);
                recyclerView_Trending = (RecyclerView) layout.findViewById(R.id.top_paid6);
                recyclerView_Trending.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_Trending.setLayoutInformationID(R.layout.apps_show_information);
                adapter_Trending.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                        startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_Trending.setAdapter(adapter_Trending);
            }
            else if (bundle.getInt("position")==2)
            {
                layout = inflater.inflate(R.layout.fragment_my1, container, false);
                recyclerView_topPaid = (RecyclerView) layout.findViewById(R.id.top_paid1);
                recyclerView_topPaid.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_topPaid.setLayoutInformationID(R.layout.apps_show_information);
                adapter_topPaid.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                      startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_topPaid.setAdapter(adapter_topPaid);
            }
            else if (bundle.getInt("position")==3)
            {
                layout = inflater.inflate(R.layout.fragment_my2, container, false);
                recyclerView_topFree = (RecyclerView) layout.findViewById(R.id.top_paid2);
                recyclerView_topFree.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_topFree.setLayoutInformationID(R.layout.apps_show_information);
                adapter_topFree.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                        startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_topFree.setAdapter(adapter_topFree);
            }
            else  if (bundle.getInt("position")==4)
            {
                layout = inflater.inflate(R.layout.fragment_my3, container, false);
                recyclerView_topGrossing = (RecyclerView) layout.findViewById(R.id.top_paid3);
                recyclerView_topGrossing.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_topGrossing.setLayoutInformationID(R.layout.apps_show_information);
                adapter_topGrossing.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                        startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_topGrossing.setAdapter(adapter_topGrossing);
            }
            else  if (bundle.getInt("position")==5)
            {
                layout = inflater.inflate(R.layout.fragment_my4, container, false);
                recyclerView_topNewPaid = (RecyclerView) layout.findViewById(R.id.top_paid4);
                recyclerView_topNewPaid.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_topNewPaid.setLayoutInformationID(R.layout.apps_show_information);
                adapter_topNewPaid.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                        startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_topNewPaid.setAdapter(adapter_topNewPaid);
            }
            else if (bundle.getInt("position")==6)
            {
                layout = inflater.inflate(R.layout.fragment_my5, container, false);
                recyclerView_topNewFree = (RecyclerView) layout.findViewById(R.id.top_paid5);
                recyclerView_topNewFree.setLayoutManager(new LinearLayoutManager(getActivity()));
                adapter_topNewFree.setLayoutInformationID(R.layout.apps_show_information);
                adapter_topNewFree.setClickListener(new RecyclerListViewAdapter.ClickListener() {
                    @Override
                    public void itemClick(View v, int position) {
                        startActivity(new Intent(getActivity(),SubActivity.class));
                    }
                });
                recyclerView_topNewFree.setAdapter(adapter_topNewFree);
            }
            else  layout = inflater.inflate(R.layout.other_page, container, false);
            return layout;
        }
        private List<InformationApps> getData_topPaid() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"name_app1", "name_app2", "name_app3", "name_app4", "name_app5", "name_app6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"price1", "price2", "price3", "price4", "price5", "price6"};
            float[] rating ={(float) 2.5,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
        private List<InformationApps> getData_topFree() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"name_app1", "name_app2", "name_app3", "name_app4", "name_app5", "name_app6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"0", "0", "0", "0", "0", "0"};
            float[] rating ={0,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
        private List<InformationApps> getData_topGrossing() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"Grossing", "Grossing2", "Grossing3", "Grossing4", "Grossing5", "Grossing6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"price1", "price2", "price3", "price4", "price5", "price6"};
            float[] rating ={(float) 2.5,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
        private List<InformationApps> getData_topNewPaid() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"topNewPaid1", "topNewPaid2", "topNewPaid3", "ntopNewPaid4", "topNewPaid5", "topNewPaid6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"price1", "price2", "price3", "price4", "price5", "price6"};
            float[] rating ={(float) 2.5,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
        private List<InformationApps> getData_topNewFree() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"_topNewFree1", "_topNewFree2", "_topNewFreep3", "n_topNewFree4", "_topNewFree5", "n_topNewFree6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"0", "0", "0", "0", "0", "0"};
            float[] rating ={(float) 2.5,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
        private List<InformationApps> getData_Trending() {
            List<InformationApps> data = new ArrayList<>();
            int r= R.drawable.ic_apps;
            int[] avatar_appId = {r, r, r, r, r, r};
            String[] name_app = {"Trending1", "Trending2", "Trending3", "nTrending4", "Trending5", "Trending6"};
            String[] name_author = {"name_autor1", "name_autor2", "name_autor3", "name_autor4", "name_autor5", "name_autor6"};
            String[] price = {"price1", "price2", "price3", "price4", "price5", "price6"};
            float[] rating ={(float) 2.5,(float)3.5,(float)2.5,(float)4.5,(float)2.5,(float)2.5};
            for (int i = 0; i < 100; i++) {
                data.add(new InformationApps( avatar_appId[i%rating.length],name_app[i%rating.length],name_author[i%rating.length],rating[i%rating.length],price[i%rating.length]));
            }
            return data;
        }
    }
}
