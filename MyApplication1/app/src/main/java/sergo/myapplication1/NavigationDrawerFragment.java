package sergo.myapplication1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    private RecyclerView recyclerView;
    public static final String PREF_FILE_NAME = "testpref";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private ActionBarDrawerToggle mDrowerToggle;
    private DrawerLayout mDrowerLayout;
    private boolean mUserLernedDrawer;
    private boolean mFromSevedInstanseState;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLernedDrawer = Boolean.valueOf(readFromPreferense(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
        if (savedInstanceState != null) mFromSevedInstanseState = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SergoAdapter adapter = new SergoAdapter(getActivity(), getData());
        adapter.setClickListener(new SergoAdapter.ClickListener() {
            @Override
            public void itemClick(View v, int position) {
                if(position==2) startActivity(new Intent(getActivity(),SubActivity.class));
            }
        });
        recyclerView.setAdapter(adapter);
        return layout;
    }

    private List<Information> getData() {
        List<Information> data = new ArrayList<>();
        int[] icons = {R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3
                , R.drawable.ic_4, R.drawable.ic_5, R.drawable.ic_6};
        String[] titles = {"Store home", "My apps", "Shop apps", "My wishlist", "People", "Myaccount"};
        for (int i = 0; i < titles.length; i++) {
            data.add(new Information(icons[i], titles[i]));
        }
        return data;
    }


    public void setUp(int fragment_navigation_drawer, DrawerLayout viewById, Toolbar toolbar) {
        View conteinerView = getActivity().findViewById(fragment_navigation_drawer);
        mDrowerLayout = viewById;
        mDrowerToggle = new ActionBarDrawerToggle(getActivity(), mDrowerLayout, toolbar, R.string.drower_open, R.string.drower_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Toast.makeText(getActivity(), "Opened Drawer", Toast.LENGTH_SHORT).show();
                if (!mUserLernedDrawer) {
                    mUserLernedDrawer = true;
                    saveToPreferense(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLernedDrawer + "");
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
                Toast.makeText(getActivity(), "Closed Drawer", Toast.LENGTH_SHORT).show();
                getActivity().invalidateOptionsMenu();
            }
        };
        if (!mUserLernedDrawer && !mFromSevedInstanseState) {
            mDrowerLayout.openDrawer(conteinerView);
        }
        mDrowerLayout.setDrawerListener(mDrowerToggle);
        mDrowerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrowerToggle.syncState();
            }
        });
    }

    public void saveToPreferense(Context context, String preferenseNeme, String preferenseValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenseNeme, preferenseValue);
        editor.commit();
    }

    public static String readFromPreferense(Context context, String preferenseNeme, String preferenseValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenseNeme, preferenseValue);
    }
}
