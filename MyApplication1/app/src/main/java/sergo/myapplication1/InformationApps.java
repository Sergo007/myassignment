package sergo.myapplication1;

/**
 * Created by Sergo on 01.02.2015.
 */
public class InformationApps {
    private String name_app;
    private String name_author;
    private String price;
    private float rating;
    private int avatar_appId;

    public InformationApps(int avatar_appId, String name_app,String name_author, float rating, String price)
    {
        this.avatar_appId=avatar_appId;
        this.name_app=name_app;
        if(price=="0")
            this.price="FREE";
        else this.price=price;
        this.rating=rating;
        this.name_author=name_author;
    }
    public String getName_app(){return name_app;}
    public String getPrice(){return price;}
    public String getName_author(){return name_author;}
    public float getRating(){return rating;}
    public int getAvatar_appId(){return avatar_appId;}
    public void setName_app(String name_app){this.name_app=name_app;}
    public void setName_author(String name_author){this.name_author=name_author;}
    public void setPrice(String price){ if(price=="0") this.price="FREE"; else this.price=price;}
    public void setRating(float rating){this.rating=rating;}
    public void setAvatar_appId(int avatar_appId){this.avatar_appId=avatar_appId;}
}
