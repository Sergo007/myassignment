package sergo.myapplication1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sergo on 29.01.2015.
 */
public class SergoAdapter extends RecyclerView.Adapter<SergoAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private ClickListener clickListener;
    List<Information> data = Collections.emptyList();

    public SergoAdapter(Context context, List<Information> data) {
        this.context=context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }
    public void setClickListener(ClickListener clickListener){this.clickListener=clickListener;}
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Information current = data.get(position);
        holder.title.setText(current.title);
        holder.icon.setImageResource(current.iconId);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView icon;


        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.listText);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);


        }


        @Override
        public void onClick(View v) {
            if(clickListener!=null) clickListener.itemClick(v,getPosition());
        }
    }
    public interface ClickListener{
        public void itemClick(View v, int position);
    }
}
